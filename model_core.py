# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 17:11:30 2020

@author: oagha
"""

from __future__ import division
import pandas as pd
import sys
import pyomo.environ as pyo

def build_model(year, month, day,
              max_discharge_cap, max_charge_cap, discharge_energy_cap, 
              ac_ac_efficiency, max_daily_discharge,
              lbmp_dict):
    '''
    This function takes in the input data for the model and builds the linear programming model
    :params: year (str) the year of the prices that we want
             month (str) the month of the prices that we want
             day (str) the day of the prices that we want
             max_discharge_cap (float) Maximum discharge power capacity (kW) 
             max_charge_cap (float) Maximum charge power capacity (kW) 
             discharge_energy_cap (float) Discharge energy capacity (kWh) 
             ac_ac_efficiency (float) AC-AC Round-trip efficiency 
             max_daily_discharge (float) Maximum daily discharged energy limit (kWh)
             lbmp_dict (dictionary) pricing data for each timestamp ($/kWh)
    :return: the built model as an object
    '''
    
    model = pyo.ConcreteModel()

    
    
    #in our model, one period is one hour. The two terms will be used interchangebly.
    
    periods1=list(range(0,25))
    
    periods2=list(range(0,24))
    
    #periods start at index 0, so the first hour in the day is hour 0. The last hour in the day is hour 23  
    
    # declare decision variables
    #x represents the amount of energy held the battery at start of a period.
    model.x = pyo.Var(periods1,bounds=(0,discharge_energy_cap))
    
    #y represents how many kwh are charged in a period
    model.y = pyo.Var(periods2,bounds=(0,max_charge_cap))
    
    #z represents how many kwh are discharged in a period
    model.z = pyo.Var(periods2,bounds=(0,max_discharge_cap))
    
    # declare objective
    #values for sense: maximize is -1, minimize is 1
    #the objective is to maximize profit -> maximize discharge revenue - charge costs
    model.obj = pyo.Objective(expr=sum(lbmp_dict[pd.to_datetime(year+'-'+month+'-'+day+' '+str(i)+':00:00')]*
                                                 ((ac_ac_efficiency*model.z[i]) - model.y[i]) 
                                                 for i in periods2),
                                                 sense = -1)
    
    #set constraints
    model.cons = pyo.ConstraintList()
    
    #at the start of the first period, the battery's state of energy is 0
    model.cons.add(model.x[0]==0)
    
    for i in periods2:
        
        #a battery may not charge and discharge at the same instantaneous point in time. 
        model.cons.add(expr=(model.y[i] / max_charge_cap) + (model.z[i] / max_discharge_cap) <=1)
        
        #inventory constraint that relates the energy held in the battery with the amounts charged and discharged
        model.cons.add(expr=(model.x[i+1]==model.x[i]+model.y[i]-model.z[i]))
    
    #maximum daily discharged energy limit may not be exceeded 
    model.cons.add(sum(model.z[i] for i in periods2)<=max_daily_discharge)
    
    return model