"""
Created on Thu Feb 20 11:03:59 2020

@author: oagha
"""

from __future__ import division
import pandas as pd
import sys

import pyomo.environ as pyo

from data_loader import get_data, get_model_variables
from model_core import build_model
from model_utils import run_model, print_model_summary

# Reading Model Inputs
input_filename = 'input_file.csv'
year, month, day, max_discharge_cap, max_charge_cap, discharge_energy_cap, ac_ac_efficiency, max_daily_discharge = get_model_variables(input_filename)

# Loading Model Data
lbmp_dict = get_data(input_filename)

# Output destination
destination_filename = 'output.txt'

# Builds Model
model = build_model(year, month, day, 
                      max_discharge_cap, max_charge_cap, discharge_energy_cap,
                      ac_ac_efficiency, max_daily_discharge,
                      lbmp_dict)

# Run model
r = run_model(model)

# If model runs succesfully
if (r == 1):
    
    print ('Model has run, printing summary...')
    
    # Print Summary to file
    p = print_model_summary(model, year, month, day, 
              ac_ac_efficiency,
              lbmp_dict, destination_filename)
    
    if (p == 1):
    
        print('Model summary has been saved at: ' + destination_filename)


