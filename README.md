# Blueprint Power Revenue Optimization

**Problem Description**
The problem is to optimize revenue generation from a battery storage system that
is performing energy arbitrage by participating in the NYISO day ahead energy 
market. The model should optimize the battery’s charge and discharge 
activities to maximize revenue for the given day. The price/cost of electricity
will be the Location Based Marginal Price (LBMP), and this data is provided. 

**My approach to the problem**
The first step that I had to take was to understand the problem completely.
Initially, I did not fully understand some of the terms related to 
batteries/electricity, and how these terms affected the physical operation of
the battery system. 

I looked up these terms online using a couple of different
resources. 2 of these resources are in this repository and are 
MIT_summary_battery_specifications.pdf and Battery Definitions - Battery University.pdf.
In addition, I asked a handful of electrical engineers about some of these terms.
In order to confirm my understanding of the system, I arranged a call with Conor.
This call was extremely helpful. 

After understanding the problem, the technical 
solution of a linear program seemed appropriate. The formulation of the linear
program can be found in Formulation.docx. After formulating the problem, I made myself
familiar with the open sourced language Pyomo, which is used by the Blueprint Power team.
I was then able to code the model using Pyomo.

In order to more accurately reflect a real-life battery system, there are a few
more aspects that could be added to the model. 

In the original problem description provided by Blueprint Power, 
it is assumed that there is no battery degredation.
However, real life batteries do degrade over time. If this model were to be run
over a longer time period, battery degredation should definitely be taken into
account. 

It is also the case that the battery is not used for any purpose other than 
performing energy arbitrage. However, a battery in real-life may need to be used 
intermittently. This could interfere in the arbitrage activity and reduce profits.

The pricing data is at the level of granularity of an hour. If more granular pricing
data were given, the model, after some slight tweaks, would be able to produce a 
more detailed and low level charge/discharge schedule. 

Below are the instructions for running the model on your system.

**1- Clone the repo and install Anaconda**
install Anaconda at https://anaconda.org/


**2- Open Anaconda Prompt / Terminal**

Windows: Open the Anaconda Prompt (Click Start, select Anaconda Prompt)

macOS: Open Launchpad, then open terminal or iTerm.

Linux–CentOS: Open Applications - System Tools - terminal.

Linux–Ubuntu: Open the Dash by clicking the upper left Ubuntu icon, then type “terminal”.

**3- Create environment and load dependencies**

Copy and paste each line below into the Anaconda prompt/Terminal

`conda create --name revenue_optimization`

`conda activate revenue_optimization`

`conda install -c conda-forge pandas`

`conda install -c conda-forge pyomo`

`conda install -c conda-forge glpk`

Since there are only 3 installation commands, a requirements.txt file is not needed.

**4- Insert inputs into input file**
Before running the model, you may want to modify the input data. This can be done 
by editing the 'Value' column of input_file.csv. Do not modify the 'Field' and 
'Category' columns. The following are the input parameters: 

name ->  the name of the zone for the pricing data to be taken from

year -> the year that the pricing data should be for; unless more data is provided,
the model will only be able to run for the year 2017

month -> the month that the pricing data should be for; ex:08

day -> the day that the pricing data should be for; ex:01

max_discharge_cap -> Maximum discharge power capacity (kW); the maximum rate at
which power can be discharged from the battery

max_charge_cap -> Maximum charge power capacity (kW); the maximum rate at which
power can be charged to the battery

discharge_energy_cap -> Discharge energy capacity (kWh); the maximum amount of 
energy that the battery can hold

ac_ac_efficiency -> AC-AC Round-trip efficiency; the fraction of energy that is 
received by the grid when discharging 1 unit of energy from the battery. 

max_daily_discharge -> Maximum daily discharged energy limit (kWh); the sum 
total of the energy discharged in a day must be less than this



**5- Run runner function**
In order to build the optimization model, run it, and print results to an output
file, you should run the runner function: `python runner.py`

**6- Results**
The results of the model will be printed to a file called output.txt. This file
will contain the following outputs:

-Total revenue generation for the given day

-Total battery storage charging cost for the given day

-Total net profit

-Total battery storage discharged throughput for the given day

-Energy stored in the battery at each timestamp

-Energy charged and discharged at each timestamp

-LBMP for each timestamp

-Revenue (or cost, if the battery is charging) for each timestamp


**7- Interpret Results**

*note that the following conclusions are drawn from running the model 
using the default input file

For optimal performance, 
we should charge the battery 100kWh in hour 3 (4th hour,
since index starts at 0). We should also charge the battery 100kWh in hour 4.

In hour 16, we should discharge 100kWh into the grid. In hour 17, we should 
discharge 100kWh into the grid. The net profit will be $7.61.

The model ran very quickly (.11 seconds), so there is the capability to scale this 
problem to many batteries over many more time periods. This is backed up by
theory, as linear programs are generally easily scalable and efficient to solve for 
large problems.


