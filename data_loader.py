import pandas as pd

def get_data(input_filename):
    '''
    This function takes in the input filename sheet provided and creates a dictionary containing pricing data
    :params: input_filename is the csv file name as a string (INCLUDE csv extension)
    :return: lbmp_dict (dictionary) pricing data for each timestamp ($/kWh)
    '''
    
    # read excel input file into df
    df = pd.read_csv(input_filename)
    
    # convert df to dict
    path_df = df.loc[df['Category'] == 'path', ['Field', 'Value']].set_index('Field')
    path_dict = path_df.to_dict()['Value']
    
    # parse dict to variables
    name = path_dict['name']
    year = path_dict['year']
    month = path_dict['month']
    day = path_dict['day']
    
    if (int(month) <= 9):
        month = '0' + month

    if (int(day) <= 9):
        day = '0' + day
    
    # create path
    path_first =year+"_NYISO_LBMPs/" + year + "_NYISO_LBMPs/" + year + month + "01damlbmp_zone_csv/"
    path = path_first + year + month + day + "damlbmp_zone.csv"
    
    # load data into df and pre-process
    df=pd.read_csv(path, usecols=["Time Stamp","Name","LBMP ($/MWHr)"]) #narrow down
    df.head()
    df=df[df["Name"]==name]  #filter
    df=df.reset_index(drop=True)  #reset index
    df=df.rename(columns={"Time Stamp": "time_stamp", "LBMP ($/MWHr)": "lbmp"}) #rename cols
    df["time_stamp"]=pd.to_datetime(df["time_stamp"]) #parse date

    #adjust price to be per kwh instead of per mwh
    df["lbmp"]=df["lbmp"] / 1000
    
    # convert df to dict
    lbmp_dict=df.set_index('time_stamp')['lbmp'].to_dict()
    
    return lbmp_dict

def get_model_variables(input_filename):
    '''
    This function takes in the input filename sheet provided and returns a tuple of the input variables,
    which are year, month, day, max_discharge_cap, max_charge_cap, discharge_energy_cap, 
    ac_ac_efficiency, and max_daily_discharge.
    :params: input_filename is the csv file name as a string (INCLUDE csv extension)
    :return: year (str) the year of the prices that we want
             month (str) the month of the prices that we want
             day (str) the day of the prices that we want
             max_discharge_cap (float) Maximum discharge power capacity (kW) 
             max_charge_cap (float) Maximum charge power capacity (kW) 
             discharge_energy_cap (float) Discharge energy capacity (kWh) 
             ac_ac_efficiency (float) AC-AC Round-trip efficiency 
             max_daily_discharge (float) Maximum daily discharged energy limit (kWh) 
    '''
    # read excel input file into df
    df = pd.read_csv(input_filename)
    
    # vars related to key
    date_df = df.loc[df['Category'] == 'path', ['Field', 'Value']].set_index('Field')
    date_dict = date_df.to_dict()['Value']
    
    # parse dict to variables
    year = date_dict['year']
    month = date_dict['month']
    day = date_dict['day']
    
    if (int(month) <= 9):
        month = '0' + month

    if (int(day) <= 9):
        day = '0' + day
    
    
    # convert df to dict
    var_df = df.loc[df['Category'] == 'model', ['Field', 'Value']].set_index('Field')
    var_dict = var_df.to_dict()['Value']

    #Maximum discharge power capacity (kW) 
    max_discharge_cap = float(var_dict['max_discharge_cap'])

    #Maximum charge power capacity (kW) 
    max_charge_cap = float(var_dict['max_charge_cap'])

    #Discharge energy capacity (kWh) 
    discharge_energy_cap = float(var_dict['discharge_energy_cap'])

    #AC-AC Round-trip efficiency (%) 
    ac_ac_efficiency = float(var_dict['ac_ac_efficiency'])

    #Maximum daily discharged energy limit (kWh) 
    max_daily_discharge = float(var_dict['max_daily_discharge'])

    return year, month, day, max_discharge_cap, max_charge_cap, discharge_energy_cap, ac_ac_efficiency, max_daily_discharge
