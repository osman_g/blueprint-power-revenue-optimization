# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 17:32:38 2020

@author: oagha
"""

from __future__ import division
import pandas as pd
import sys
import pyomo.environ as pyo

def run_model(model):
    '''
    This function takes in the built model runs it using the glpk solver
    :params: the built model object
    :return: 1
    '''
    opt = pyo.SolverFactory('glpk')
    opt.solve(model).write()
    return 1

def print_model_summary(model, year, month, day, 
              ac_ac_efficiency,
              lbmp_dict, destination_filename):
    
    '''
    This function takes in the model object after it has been run, the date details, 
    the ac_ac_efficiency, the model's pricing data, and the file that the results should be written to.
    This function writes the results to the specified file
    :params: model (object) model object that has been built and run
             year (str) the year of the model's timeframe
             month (str) the month of the model's timeframe
             day (str) the day of the model's timeframe
             ac_ac_efficiency (float) AC-AC Round-trip efficiency
             lbmp_dict (dictionary) pricing data for each timestamp ($/kWh)
             destination_filename is the csv file name as a string (INCLUDE csv extension)
    :return: 1
    '''
    
    periods1=list(range(0,25))
    periods2=list(range(0,24))
    
    file = open(destination_filename, "w") 
    
    total_revenue=sum(lbmp_dict[pd.to_datetime(year+'-'+month+'-'+day+' '+str(i)+':00:00')]*((ac_ac_efficiency*model.z[i].value)) for i in periods2)
    
    total_cost=sum(lbmp_dict[pd.to_datetime(year+'-'+month+'-'+day+' '+str(i)+':00:00')]*( model.y[i].value) for i in periods2)
        
    file.write("Total revenue generation ($) for the given day: {} \n \n".format(str(total_revenue)))
    
    file.write("Total battery storage charging cost ($) for the given day: {} \n \n".format(str(total_cost)))
    
    file.write("Total net profit ($): {} \n \n".format(getattr(model, 'obj')()))
    
    file.write("Total battery storage discharged throughput (kWh) for the given day {} \n \n".format(str(sum( model.z[i].value for i in periods2))))
    
     
    file.write("Energy (kWh) stored in the battery at each timestamp: \n")
    for i in periods1:
        file.write("at {}, the battery has {} kWh \n".format(year+'-'+month+'-'+day+' '+str(i)+':00:00',model.x[i].value))
    
    file.write("\n")
    
    file.write("Energy charged and discharged (kWh) at each timestamp: \n")
    for i in periods2:
        file.write("during the hour of {}, {} kWh was charged \n".format(year+'-'+month+'-'+day+' '+str(i),model.y[i].value))
        file.write("during the hour of {}, {} kWh was discharged \n \n".format(year+'-'+month+'-'+day+' '+str(i),model.z[i].value))
    
   
    
    file.write("\n")
    
   
    file.write("The LBMP for each timestamp ($): \n")
    for i in periods2:
        file.write("during the hour of {}, the LBMP is {} \n".format(year+'-'+month+'-'+day+' '+str(i), lbmp_dict[pd.to_datetime(year+'-'+month+'-'+day+' '+str(i)+':00:00')]))
    
    file.write("\n")
    
    file.write("The revenue (or cost, if the battery is charging) for each timestamp ($): \n")
    for i in periods2:
        file.write("during the hour of {}: the net revenue was {} \n".format(year+'-'+month+'-'+day+' '+str(i), lbmp_dict[pd.to_datetime(year+'-'+month+'-'+day+' '+str(i)+':00:00')]*((ac_ac_efficiency*model.z[i].value) - model.y[i].value)))
    
    
    
    file.close()
    return 1